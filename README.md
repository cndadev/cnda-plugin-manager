# README #

This is a simple gradle project to pull all of the CNDA dependencies from the XNAT maven server and install them in the local plugins directory. Because the default tasks are defined in the build.gradle, running it with default settings is as simple as calling:
```
#!bash

./gradlew
```

The location to install the plugins is governed by specifying the 'pluginDir' property to gradle. If this property is not found, the directory used will be the 'plugins' subdirectory of the directory specified by an XNAT\_HOME environment variable. If *that* is not found either, the project will finally default to just use /data/CNDA/home/plugins. Additionally, you can specify specific versions of the plugins to be used by passing gradle properties. The property name to use should match the variable definitions at the top of the build.gradle. So, a more complicated invocation could look like:
```
#!bash

./gradlew -PpluginDir=/my/path/to/plugins -PvOptical=1.0.1-SNAPSHOT -PvWmh=1.1.0
```

This script also supports running in "dev mode", which modifies which versions of the plugins will be installed. Instead of using the released versions of the plugins (e.g. 1.0.0, 1.0.1, 1.1.0, etc.), the script will pull the last deployed version of the plugin (which in most cases will be a snapshot version). However, specific versions can still be overwritten as described earlier. To enable this feature, add the 'dev' property with value 'true'. For example:
```
#!bash

./gradlew -Pdev=true -PvPup=1.0.1 -PpluginDir=/my/path/to/plugins
```

This would install the latest builds of all of the CNDA plugins, except the PUP plugin, which would use the 1.0.1 release.
